from django.shortcuts import render, get_object_or_404
from django.views import generic

from .models import Article, Category, Comment


class ArticleListView(generic.ListView):
    model = Article
    template_name = 'article.html'
    queryset = Article.objects.filter(title__icontains='2')

    # queryset = Article.objects.all()
    # equivalent contexte avec def
    # context_object_name = 'article'


class ArticleDetailView(generic.DetailView):
    model = Article
    template_name = 'article_detail.html'


def article_detail(request, primary_key):
    article = get_object_or_404(Article, pk=primary_key)

    context = {
        'article': article
    }
    return render(request, 'article_detail.html', context=context)


def index(request):
    num_articles = Article.objects.all().count()
    num_categories = Category.objects.all().count()
    articles = Article.objects.all()

    context = {
        'num_articles': num_articles,
        'num_categories': num_categories,
        'articles_list': articles
    }
    return render(request, 'index.html', context=context)


def category_index(request):
    categories = Category.objects.all()

    context = {
        'categories_list': categories

    }
    return render(request, 'categories.html', context=context)


def comment_index(request):
    comment = Comment.objects.all()

    context = {
        'comment_list': comment

    }
    return render(request, 'comment.html', context=context)


class CategoryListView(generic.ListView):
    model = Category
    context_object_name = 'categories'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['test'] = 'un test en texte'
        context['number'] = 53
        return context

    def get_queryset(self):
        return Category.objects.filter(name__contains=self.kwargs['name'])
