from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('categories/', views.category_index, name='categories'),
    path('comment/', views.comment_index, name='comment'),

    path('list/', views.ArticleListView.as_view(), name='uneliste'),
    path('<int:pk>', views.ArticleDetailView.as_view(), name='article-detail'),

    #path('<str:slug>', views.ArticleDetailView.as_view(), name='article-detail'),

]