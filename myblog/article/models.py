from django.db import models
from django.urls import reverse
from django.utils.safestring import mark_safe


class Category(models.Model):
    """Cet objet représente une catégorie ou un genre littéraire."""
    name = models.CharField(max_length=200, help_text='Enter an article category (ex : Actualité)')
    # image_url = models.CharField(max_length=2048, help_text='Enter an image url for the category', null=True, blank=True)
    image_url = models.ImageField(upload_to='images/', help_text='Enter an image url for the article', null=True,
                                  default='')

    class Meta:
        verbose_name_plural = "Categories"
        ordering = ['name']

    def __str__(self):
        """Cette fonction est obligatoirement requise par Django.
           Elle retourne une chaîne de caractère pour identifier l'instance de la classe d'objet."""
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=200, help_text='Enter te titles', verbose_name="titres'")
    summary = models.TextField(max_length=1000, help_text='Enter a brief description of the article',
                               verbose_name="resumé'")
    content = models.TextField(help_text='Enter the article content', verbose_name="contenu'")
    slug = models.SlugField(max_length=300, help_text='Enter the article slug', null=True, blank=True, default="")
    cover_image_url = models.ImageField(upload_to='images/', help_text='Enter an image url for the article', null=True,
                                        default='')
    thumbnail_image_url = models.CharField(max_length=2048, help_text='Enter an image url for the article', null=True,
                                           default='')
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    publish_time = models.DateTimeField(null=True)
    visibily = models.BooleanField(default=False, help_text='Visible de l article')
    file = models.FileField(upload_to='files/', help_text='ajouter un fichier', null=True, blank=True)
    category =  models.ForeignKey(Category, help_text='Select a category for this article', on_delete=models.SET_NULL,
                                 null=True, default='', blank=True)

    @property
    def image_preview(self):
        if self.cover_image_url:
            return mark_safe("<img src={} width='300' height='200' />".format(self.cover_image_url))
        return ""

    def __str__(self):
        return self.title


def get_absolute_url(self):
    """Cette fonction est requise pas Django, lorsque vous souhaitez détailler le contenu d'un objet."""
    return reverse('article-detail', args=[str(self.id)])


class Comment(models.Model):
    author_name = models.CharField(max_length=200)
    content = models.TextField(help_text='Enter the article content')
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    article = models.ForeignKey(Article, help_text='Select a article ', on_delete=models.SET_NULL,
                                null=True)

    def __str__(self):
        # return self.author_name
        return self.content
